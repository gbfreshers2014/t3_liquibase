CREATE DATABASE  IF NOT EXISTS `t3bi`; 
USE `t3bi`;

DROP TABLE IF EXISTS `DailyReport`;

CREATE TABLE `DailyReport` (
  `employeeID` varchar(254) DEFAULT NULL,
  `itemId` int(11) NOT NULL,
  `dueDate` date DEFAULT NULL,
  `pendingDays` varchar(45) DEFAULT NULL,
  `reportDate` date NOT NULL,
  PRIMARY KEY (`itemId`,`reportDate`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;


DROP TABLE IF EXISTS `weeklyReport1`;

CREATE TABLE `weeklyReport1` (
  `itemType` varchar(50) NOT NULL,
  `countOfItem` int(11) DEFAULT NULL,
  `reportDate` date NOT NULL,
  PRIMARY KEY (`reportDate`,`itemType`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;


DROP TABLE IF EXISTS `weeklyReport2`;

CREATE TABLE `weeklyReport2` (
  `employeeId` varchar(200) NOT NULL,
  `averageReturnTime` int(11) DEFAULT NULL,
  `reportDate` date NOT NULL,
  PRIMARY KEY (`reportDate`,`employeeId`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
