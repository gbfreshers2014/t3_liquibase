########################################################
# Create the TEAM 3 Inventory Management System database
########################################################

CREATE DATABASE IF NOT EXISTS t3;

USE t3;

###############
# Create tables
###############

# Table INVENTORY_STATUS, to record STATUS NUMBER FOR DIFFERENT TYPES OF INVENTORY STATUS

CREATE TABLE `INVENTORY_STATUS` (
  `statusNumber` int(11) NOT NULL,
  `statusDescription` varchar(20) NOT NULL,
  `createdOn` datetime DEFAULT NULL,
  `updatedOn` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  KEY `STATUS_NUMBER` (`statusNumber`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;


INSERT INTO INVENTORY_STATUS (statusNumber,statusDescription,createdOn,updatedOn) VALUES (1,'AVAILABLE',now(),now());
INSERT INTO INVENTORY_STATUS (statusNumber,statusDescription,createdOn,updatedOn) VALUES (2,'UNAVAILABLE',now(),now());
INSERT INTO INVENTORY_STATUS (statusNumber,statusDescription,createdOn,updatedOn) VALUES (3,'DELETED',now(),now());

# Table INVENTORY, to record inventory details

CREATE TABLE `INVENTORY` (
  `itemId` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `itemName` varchar(50) NOT NULL,
  `itemType` varchar(50) NOT NULL,
  `description` varchar(200) DEFAULT NULL,
  `namespaceId` int(11) NOT NULL DEFAULT '0',
  `createdOn` datetime DEFAULT NULL,
  `createdBy` varchar(254) NOT NULL,
  `updatedBy` varchar(254) DEFAULT NULL,
  `updatedOn` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `status` int(11) DEFAULT NULL,
  PRIMARY KEY (`itemId`),
  KEY `STATUS` (`status`),
  CONSTRAINT `fk_INVENTORY_1` FOREIGN KEY (`status`) REFERENCES `INVENTORY_STATUS` (`statusNumber`) ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=latin1;


# Table TRANSACTION_STATUS, to record STATUS NUMBER FOR EACH KIND OF TRANSACTION

CREATE TABLE `TRANSACTION_STATUS` (
  `statusNumber` int(11) NOT NULL,
  `statusDescription` varchar(20) NOT NULL,
  `createdOn` datetime DEFAULT NULL,
  `updatedOn` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  KEY `STATUS_NUMBER` (`statusNumber`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;


INSERT INTO TRANSACTION_STATUS (statusNumber,statusDescription,createdOn,updatedOn) VALUES (1,'ISSUE',NOW(),NOW());
INSERT INTO TRANSACTION_STATUS (statusNumber,statusDescription,createdOn,updatedOn) VALUES (2,'RETURN',NOW(),NOW());


# Table TRANSACTION, to record ISSUE & RETURN TRANSACTIONS OF AN ITEM

CREATE TABLE `TRANSACTION` (
  `transactionId` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `itemId` int(10) unsigned NOT NULL,
  `employeeId` varchar(254) NOT NULL,
  `namespaceId` int(11) NOT NULL,
  `status` int(11) NOT NULL,
  `issuedBy` varchar(254) NOT NULL,
  `issuedOn` datetime DEFAULT NULL,
  `expectedReturn` datetime DEFAULT NULL,
  `returnedOn` datetime DEFAULT NULL,
  `returnedTo` varchar(254) DEFAULT NULL,
  PRIMARY KEY (`transactionId`),
  KEY `EMPLOYEE_ID` (`employeeId`),
  KEY `fk_TRANSACTION_1` (`status`),
  CONSTRAINT `fk_TRANSACTION_1` FOREIGN KEY (`status`) REFERENCES `TRANSACTION_STATUS` (`statusNumber`) ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
