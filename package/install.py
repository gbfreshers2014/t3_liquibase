#! /usr/bin/env python

import os
import sys

service_inventory_config_file = "/home/gb/conf/team3-inventory-management-conf/service_inventory.db.properties"
service_inventory_bi_config_file = "/home/gb/conf/team3-inventory-management-conf/service_inventory_bi.db.properties"
mysql_driver_path = "/home/gb/lib/team3-inventory-management/mysql-connector-java-*.jar"
config_path = "/home/gb/lib/team3-inventory-management/team3-inventory-management-*.jar"
liquibase = "/home/gb/lib/team3-inventory-management/liquibase-core*"

classpath = "`ls %s`:`ls %s`" % (mysql_driver_path, config_path)

def parse_config_file(filename):
    """Parse configuration file and return a hash"""
    result = dict()
    file = open(filename, "rb")
    while True:
        line = file.readline()
        if not line:
            break
        line = line.rstrip("\r\n")
        if line != "":
            [key, value] = line.split("=")
            result[key] = value
    return result

def liquibase_update(config_file):
    """Runs liquibase with the provided configuration file(s)"""
    config = parse_config_file(config_file)
    command = """java -jar %s --driver=%s --classpath=%s --changeLogFile=%s --url=%s --username=%s --password=%s update""" \
                % (liquibase, config["driver"], classpath, \
                   config["changeLogFile"], config["url"], \
                   config["username"], config["password"])
    print "Running command: " + command
    return os.system(command)

def main():
    """Main function"""
    retval = liquibase_update(service_inventory_config_file)
    if retval:
	sys.exit(retval)
    retval = liquibase_update(service_inventory_bi_config_file)
    if retval:
	sys.exit(retval)

if __name__ == "__main__":
    main()
